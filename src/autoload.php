<?php

namespace RODB;

// modified version of php-fig PSR-4 autoloader

class PSR4Autoloader
{
	protected array $prefixes = [];

	public function Register() : void
	{
		spl_autoload_register([$this, 'LoadClass']);
	}

	public function AddNamespace(string $prefix, string $base_dir, bool $prepend = false) : void
	{
		$prefix = trim($prefix, '\\') . '\\';
		$base_dir = rtrim($base_dir, DIRECTORY_SEPARATOR).'/';

		if (!isset($this->prefixes[$prefix]))
			$this->prefixes[$prefix] = [];

		if ($prepend)
			array_unshift($this->prefixes[$prefix], $base_dir);
		else
			array_push($this->prefixes[$prefix], $base_dir);
	}

	public function LoadClass(string $class) : string|bool
	{
		$prefix = $class;

		while (($pos = strrpos($prefix, '\\')) !== false)
		{
			$prefix = substr($class, 0, $pos+1);
			$relative = substr($class, $pos+1);
			$fh_mapped = $this->loadMappedFile($prefix, $relative);
			if ($fh_mapped)
				return $fh_mapped;

			$prefix = rtrim($prefix, '\\');
		}

		assert(false, "failed to load class `{$class}`"); 
		return false;
	}

	protected function loadMappedFile(string $prefix, string $relative) : string
	{
		assert(isset($this->prefixes[$prefix]));

		foreach ($this->prefixes[$prefix] as $dir)
		{
			$file = $dir . str_replace('\\', '/', $relative) . '.php';

			if ($this->requireFile($file))
				return $file;
		}

		assert(false, "autoloader requested class failure p:`{$prefix}` r:`{$relative}`");
		return false;
	}

	protected function requireFile(string $filename) : bool
	{
		assert(file_exists($filename));

		require $filename;
		return true;
	}
}
