<?php

define('ROWEB_START_TIME', microtime(true));
define('ROWEB_START_MEM', memory_get_usage());

error_reporting(E_ALL);
ini_set('display_errors', '1');
ini_set('date.timezone', 'America/New_York');

if (!chdir('..'))
	exit('couldnt set working directory');

require 'autoload.php';

$loader = new \RODB\PSR4Autoloader();
$loader->Register();
$loader->AddNamespace('RODB', getcwd().DIRECTORY_SEPARATOR.'classes');

function AssertFailCB(string $script, int $line, ?string $x, string $desc = '') : void
{
	$txt = "Assertion failed! {$script}:{$line} - info: {$desc}";
	\RODB\Log::Critical($txt);

	exit($txt); // in case it didn't die!
}

assert_options(ASSERT_ACTIVE, true);
assert_options(ASSERT_BAIL, false);
assert_options(ASSERT_WARNING, true);
assert_options(ASSERT_CALLBACK, 'AssertFailCB');

\RODB\Log::_init(new \RODB\FileLogger());

