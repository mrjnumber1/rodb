<?php

namespace RODB;

class CLI
{
	const ANYKEY = 'Press any key to continue...';
	public static bool $nocolor = false;
	protected static $args = [];
	protected static readonly $FGC = [
		'black' => '0;30',
		'dark_gray' => '1;30',
		'red' => '0;31',
		'light_red' => '1;31',
		'green' => '0;32',
		'light_green' => '1;32',
		'light_yellow' => '0;33',
		'yellow' => '1;33',
		'blue' => '0;34',
		'light_blue' => '1;34',
		'purple' => '0;35',
		'light_purple' => '1;35',
		'cyan' => '0;36',
		'light_cyan' => '1;36',
		'light_gray' => '0;37',
		'white' => '1;37',
	];
	protected static readonly $BGC = [
		'black' => '40',
		'red' => '41',
		'green' => '42',
		'yellow' => '43',
		'blue' => '44',
		'magenta' =>'45',
		'cyan' => '46',
		'light_gray' => '47'
	];

	protected static mixed $stdout;
	protected static mixed $stderr; 

	public static function IsCLI() : bool {
		if (defined('STDIN'))
			return true;

		if (empty($_SERVER['REMOTE_ADDR']) && !isset($_SERVER['HTTP_USER_AGENT']) && count($_SERVER['argv']) > 0)
			return true;

		return false;
	}

	public static function _init()
	{
		assert(static::IsCLI(), 'CLI class used outside of command line!!!');

		for ($i = 1; $i < $_SERVER['argc']; ++$i)
		{
			$arg = explode('=', $_SERVER['argv'][$i]);

			static::$args[$i] = $arg[0];
			if (count($arg) > 1 || !strncmp($arg[0], '-', 1))
				static::$ARGS[ltrim($arg[0], '-')] = isset($arg[1]) ? $arg[1] : true;


		}

		static::$stderr = STDERR;
		static::$stdout = STDOUT;
	}

	public static function Option(string $name, mixed $default = null) : mixed
	{
		if (!isset(static::$ARGS[$name]))
			return $default;

		return static::$ARGS[$name];
	}

	public static function SetOption(string|int $name, mixed $value) : void
	{
		if ($value === null)
		{
			if (isset(static::$ARGS[$name]))
				unset(static::$ARGS[$name]);
		}
		else
			static::$ARGS[$name] = $value;
	}

	public static function Input(string|int $prefix = '') : string
	{
		echo $prefix;
		return fgets(STDIN);
	}

	public static function Prompt() : string
	{
		$args = func_get_args();

		$options = [];
		$output = '';
		$default = null;

		$argcount = count($args);

		$required = end($args) === true;
		$required === true and --$argcount;

		switch ($argcount)
		{
		case 2:
			if (is_array($args[1]))
				[$output, $options] = $args;
			elseif (is_string($args[1]))
				[$output, $default] = $args;
			break;
		case 1:
			if (is_array($args[0]))
				$options = $args[0];
			elseif (is_string($args[0])
				$output = $args[0];
			break;
		}

		if ($output !== '')
		{
			$extra_output = '';
			if (!is_null($default))
				$extra_output = " [ Default: `{$default}` ]";
			elseif ($options !== [])
				$extra_output = ' [ '.implode(', ', $options).' ]';

			fwrite(static::$stdout, $output.$extra_output.': ');
		}

		$input = trim(static::Input()) ?: $default;

		if (empty($input) && $required)
		{
			static::Beep(3);
			static::Write('This is required!');
			static::NewLine();

			$input = forward_static_call_array([__CLASS__, 'Prompt', $args]);
		}

		if (!empty($options) && !in_array($input, $options))
		{
			static::Beep(3);
			static::Write('Invalid option!');
			static::NewLine();

			$input = forward_static_call_array([__CLASS__, 'Prompt', $args]);
		}
		return $input;
	}

	public static function Write(string|array $text = '', string $fg = null, string $bg = null) : void
	{
		if (is_array($text))
			$text = implode(PHP_EOL, $text);

		if ($fg || $bg)
			$text = static::Color($text, $fg, $bg);

		fwrite(static::$stdout, $text.PHP_EOL);
	}

	public static function Error(string|array $text = '', string $fg = null, string $bg = null) : void
	{
		if (is_array($text))
			$text = implode(PHP_EOL, $text);
		if ($fg || $bg)
			$text = static::Color($text, $fg, $bg);

		fwrite(static::$stderr, $text.PHP_EOL);
	}

	public static function Beep(int $num = 1) : void
	{
		if ($num > 3)
			$num = 3;

		echo str_repeat('\x07', $num);
	}

	public static function Wait(int $seconds = 0, bool $countdown = false) : void
	{
		if ($countdown)
		{
			$time = $seconds;
			while ($time)
			{
				fwrite(static::$stdout, $time.'...');
				sleep(1);
				$time--;
			}
			static::Write();
		}
		else
		{
			if ($seconds)
				sleep($seconds);
			else
			{
				static::Write(self::ANYKEY);
				static::Input();
			}
		}

	}

	public static function IsWindows() : void
	{
		return !strpos(strtolower(php_uname('s')), 'windows');
	}

	public static function NewLine(int $num = 1) : void
	{
		for ($i = 0; $i < $num; ++$i)
			static::Write();
	}

	public static function ClearScreen() : void
	{
		if (IsWindows())
			static::NewLine(10);
		else
			fwrite(static::$stdout, chr(27).'[H'.chr(27).'[2J');
	}

	public static function Color(string $text, string $fg, string $bg, string $format=null)
	{
		if (IsWindows() && !$_SERVER['ANSICON'])
			return $text;

		if (static::$nocolor)
			return $text;

		assert(array_key_exists($fg, static::$FGC), "Invalid CLI fg `{$fg}`");
		assert(is_null($bg) || array_key_exists($bg, static::$BGC), "invalid CLI bg color `{$bg}`");

		$str = '\033['.static::$FGC[$fg].'m';

		if (!is_null($bg))
			$str .= '\033['.static::$BGC[$bg].'m';


		if ($format == 'underline')
			$str .= '\033[4m';

		$str .= $text.'\033[0m';

		return $str;
	}

	public static function Spawn(string $call, string $output = '/dev/null') : void
	{
		if (static::IsWindows())
			pclose(popen("start /b {$call}", 'r'));
		else
			pclose(popen("{call} > {$output} &", 'r'));
	}

	public static function STDERR(mixed $fp = null) : mixed
	{
		$original = static::$stderr;

		if (!is_null($fp))
		{
			if (is_string($fp))
				$fp = fopen($fp, 'w');
			static::$stderr = $fp;
		}

		return $original;
	}

	public static function STDOUT(mixed $fp = null) : mixed
	{
		$original = static::$stdout;

		if (!is_null($fp))
		{
			if (is_string($fp))
				$fp = fopen($fp, 'w');
			static::$stdout = $fp;
		}

		return $original;
	}

}
