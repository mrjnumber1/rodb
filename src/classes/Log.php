<?php

namespace RODB;

enum LogLevel : string
{
	case EMERGENCY = 'emergency';
	case ALERT = 'alert';
	case CRITICAL = 'critical error';
	case ERROR = 'error';
	case WARNING = 'warning';
	case INFO = 'info';
	case DEBUG = '@@DEBUG@@';
}

interface Logger
{
	public function Emergency(string $msg, array $context = null) : void;
	public function Alert(string $msg, array $context = null) : void;
	public function Critical(string $msg, array $context = null) : void;
	public function Error(string $msg, array $context = null) : void;
	public function Warning(string $msg, array $context = null) : void;
	public function Info(string $msg, array $context = null) : void;
	public function Debug(string $msg, array $context = null) : void;
	public function Log(LogLevel $level, string $msg, array $context = null) : void;
}


abstract class AbstractLogger implements Logger
{
	public function Emergency(string $msg, array $context = null) : void
	{
		$this->Log(LogLevel::EMERGENCY, $msg, $context);
	}
	public function Alert(string $msg, array $context = null) : void
	{
		$this->Log(LogLevel::ALERT, $msg, $context);
	}
	public function Critical(string $msg, array $context = null) : void
	{
		$this->Log(LogLevel::CRITICAL, $msg, $context);
	}
	public function Error(string $msg, array $context = null) : void
	{
		$this->Log(LogLevel::ERROR, $msg, $context);
	}
	public function Warning(string $msg, array $context = null) : void
	{
		$this->Log(LogLevel::WARNING, $msg, $context);
	}
	public function Info(string $msg, array $context = null) : void
	{
		$this->Log(LogLevel::INFO, $msg, $context);
	}
	public function Debug(string $msg, array $context = null) : void
	{
		$this->Log(LogLevel::DEBUG, $msg, $context);
	}
}

class Log
{
	private static Logger $logger;
	public static function _init(Logger $logger)
	{
		static::$logger = $logger;
	}

	public static function Emergency(string $msg, array $context = null) : void
	{
		static::$logger->Emergency($msg, $context);
	}

	public static function Alert(string $msg, array $context = null) : void
	{
		static::$logger->Alert($msg, $context);
	}

	public static function Critical(string $msg, array $context = null) : void
	{
		static::$logger->Critical($msg, $context);
	}

	public static function Error(string $msg, array $context = null) : void
	{
		static::$logger->Error($msg, $context);
	}

	public static function Warning(string $msg, array $context = null) : void
	{
		static::$logger->Warning($msg, $context);
	}

	public static function Info(string $msg, array $context = null) : void
	{
		static::$logger->Info($msg, $context);
	}

	public static function Debug(string $msg, array $context = null) : void
	{
		static::$logger->Debug($msg, $context);
	}

}

class FileLogger extends AbstractLogger
{
	const LOGDIR = 'logs';
	const PATH = self::LOGDIR.DIRECTORY_SEPARATOR;

	private string $filename;
	private string $full_filename;
	private mixed $fp;

	public function __construct(string $filename = null)
	{
		assert(is_null($filename) || !strpbrk($filename, "\"\\?*:/@|<>"), "invalid log filename `{$filename}`");

		if (!file_exists(self::LOGDIR))
		{
			$ok = mkdir(self::LOGDIR);
			assert($ok, "unable to create log directory!");
		}
		if (is_null($filename))
		{
			$time = new \DateTime();
			$filename = $time->format('YmdHis').'.log';
			$full_filename = $this::PATH.$filename;
		}

		$this->fp = fopen($full_filename, 'a');
		assert($this->fp, "failed to open log file `{$full_filename}`");

		$this->filename = $filename;
		$this->full_filename = $full_filename;
	}

	public function __destruct()
	{
		fclose($this->fp);
	}


	public function Log(LogLevel $level, string $msg, array $context=null) : void
	{
		$time = new \DateTime();
		$timestr = $time->format('[His.u]');
		$txt = $timestr.'[';
		$exception_msg = null;
		$e = null;

		if (!is_null($context) && array_key_exists('exception', $context))
		{
			$e = $context['exception'];
			if (is_a($e, 'Exception'))
				$exception_msg = get_class($e).'(code '.$e->getCode().') at '.$e->getFile().':'.$e->getLine().' uncaught! msg: '.$e->getMessage().PHP_EOL.'stack: '.PHP_EOL.$e->getTraceAsString();
			else
				$exception_msg = 'Unknown exception `'.get_class($e).'`';
		}

		$txt .= "{$level->value}] {$msg}";

		fputs($this->fp, $txt.PHP_EOL);
		if (!is_null($e))
			fputs($this->fp, $exception_msg.PHP_EOL);

		if ($level == LogLevel::CRITICAL || $level == LogLevel::EMERGENCY)
		{
			exit($txt);
		}
	}
}

class NullLogger extends AbstractLogger
{
	public function Log(LogLevel $level, string $msg, array $context=null) : void
	{
		if ($level == LogLevel::CRITICAL || $level == LogLevel::EMERGENCY)
		{
			die;
		}
	}
}
	

